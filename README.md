科学上网docker镜像。在1080端口上开启SOCKS5 proxy。并在2080开启http proxy。

SSR服务器及其配置请自行搞定。

shadowsocksr所在git请自行搜索。

# Build
0. `git clone fq && cd fq`
0. `git clone <shadowsocksr-repo>`
0. `docker build -t fq .`


# Run
`docker run -d --restart unless-stopped --name fq --network host -v $PWD/your-config.json:/etc/sslocal.json fq`

上面`your-config.json`是你自己的配置文件。请保证在当前路径下。

没有输出的话，表示运行成功。并且以上命令会保证容器每次重启也会自动运行

**NOTE** 现在只支持1080端口作为SOCK5。
