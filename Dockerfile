FROM daocloud.io/library/ubuntu:16.04

RUN sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
RUN apt-get update && \
    apt-get install -y python python-pip npm && \
    npm install -g http-proxy-to-socks && \
    pip install honcho && \
    ln -s /usr/bin/nodejs /usr/bin/node

ADD shadowsocksr /shadowsocksr

WORKDIR /shadowsocksr/shadowsocks
ADD Procfile .

CMD honcho start
